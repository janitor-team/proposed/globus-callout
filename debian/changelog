globus-callout (4.3-2) unstable; urgency=medium

  * Make doxygen Build-Depends-Indep
  * Drop old debug symbol migration from 2017

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 05 Jul 2022 20:54:37 +0200

globus-callout (4.3-1) unstable; urgency=medium

  * Minor fixes to makefiles
  * Change to debhelper compat level 13
  * Remove override_dh_missing rule (--fail-missing is default)
  * Drop old symlink-to-dir conversion from 2014

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Mon, 14 Dec 2020 00:13:23 +0100

globus-callout (4.2-1) unstable; urgency=medium

  * Add AC_CONFIG_MACRO_DIR and ACLOCAL_AMFLAGS

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Mon, 02 Sep 2019 11:00:02 +0200

globus-callout (4.1-2) unstable; urgency=medium

  * Convert debian/rules to dh tool
  * Change to debhelper compat level 10
  * Update documentation links in README file

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 12 Jul 2019 16:34:27 +0200

globus-callout (4.1-1) unstable; urgency=medium

  * Doxygen fixes
  * Use .maintscript file for dpkg-maintscript-helper

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 27 Feb 2019 20:00:47 +0100

globus-callout (4.0-1) unstable; urgency=medium

  * Switch upstream to Grid Community Toolkit
  * First Grid Community Toolkit release
  * Move VCS to salsa.debian.org

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 16 Sep 2018 03:16:31 +0200

globus-callout (3.15-2) unstable; urgency=medium

  * Migrate to dbgsym packages
  * Support DEB_BUILD_OPTIONS=nocheck

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 06 Jul 2017 12:58:28 +0200

globus-callout (3.15-1) unstable; urgency=medium

  * GT6 update
  * Change Maintainer e-mail (fysast → physics)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 02 Sep 2016 10:53:58 +0200

globus-callout (3.14-1) unstable; urgency=medium

  * GT6 update

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 03 May 2016 13:54:44 +0200

globus-callout (3.13-5) unstable; urgency=medium

  * Set SOURCE_DATE_EPOCH and rebuild using doxygen 1.8.11
    (for reproducible build)
  * Update URLs (use toolkit.globus.org)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 16 Feb 2016 11:05:44 +0100

globus-callout (3.13-4) unstable; urgency=medium

  * Rebuild using doxygen 1.8.9.1 (Closes: #630047)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 30 Apr 2015 10:54:12 +0200

globus-callout (3.13-3) unstable; urgency=medium

  * Add Pre-Depends for dpkg-maintscript-helper

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 08 Nov 2014 20:59:36 +0100

globus-callout (3.13-2) unstable; urgency=medium

  * Properly handle symlink-to-dir conversion in doc package (Closes: #768260)
  * Enable verbose tests

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 07 Nov 2014 09:11:54 +0100

globus-callout (3.13-1) unstable; urgency=medium

  * GT6 update
  * Drop patches globus-callout-flavor.patch and globus-callout-doxygen.patch
    (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 26 Oct 2014 13:29:17 +0100

globus-callout (3.12-1) unstable; urgency=medium

  * Update to Globus Toolkit 6.0
  * Drop GPT build system and GPT packaging metadata
  * Enable checks

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 23 Sep 2014 13:49:00 +0200

globus-callout (2.4-2) unstable; urgency=low

  * Remove junk man page

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 05 Dec 2013 20:46:29 +0100

globus-callout (2.4-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.5
  * Implement Multi-Arch support
  * Rename dbg package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 10 Nov 2013 14:35:49 +0100

globus-callout (2.2-2) unstable; urgency=low

  * Add arm64 to the list of 64 bit architectures

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 26 May 2013 16:28:14 +0200

globus-callout (2.2-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.1
  * Drop patches globus-callout-ltdl-win.patch and
    globus-callout-doxygen.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 28 Apr 2012 22:30:59 +0200

globus-callout (2.1-2) unstable; urgency=low

  * Fix broken links in README file

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 24 Jan 2012 17:01:52 +0100

globus-callout (2.1-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.0
  * Drop patches globus-callout-mingw.patch and globus-callout-noflavext.patch
    (fixed upstream)
  * Make doc package architecture independent

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 27 Dec 2011 14:38:03 +0100
globus-callout (0.7-8) unstable; urgency=low

  * Use system jquery script

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 07 Jun 2011 02:33:37 +0200

globus-callout (0.7-7) unstable; urgency=low

  * Add README file
  * Use new doxygen-latex build dependency (Closes: #616220)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 26 Apr 2011 06:38:01 +0200

globus-callout (0.7-6) unstable; urgency=low

  * Converting to package format 3.0 (quilt)
  * Add new build dependency on texlive-font-utils due to changes in texlive
    packaging (epstopdf moved there) (Closes: #583044)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 04 Jun 2010 04:43:29 +0200

globus-callout (0.7-5) unstable; urgency=low

  * Update to Globus Toolkit 5.0.0
  * Add debug package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 26 Jan 2010 18:10:17 +0100

globus-callout (0.7-4) unstable; urgency=low

  * Fix rule dependencies in the debian/rules file.
  * Allow loading callouts without flavor extensions.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 13 May 2009 13:47:05 +0200

globus-callout (0.7-3) unstable; urgency=low

  * Initial release (Closes: #512959).
  * Preparing for other 64bit platforms than amd64.
  * Rebuilt to correct libltdl dependency.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 18 Apr 2009 20:17:29 +0200

globus-callout (0.7-2) UNRELEASED; urgency=low

  * Only quote the Apache-2.0 license if necessary.
  * Updated deprecated Source-Version in debian/control.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 26 Mar 2009 09:21:25 +0100

globus-callout (0.7-1) UNRELEASED; urgency=low

  * First build.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 03 Jan 2009 09:56:12 +0100
